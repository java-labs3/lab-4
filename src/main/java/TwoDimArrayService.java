public class TwoDimArrayService {

    /**
     * Given a matrix of elements
     * A =
     * {a00, a01, a02, ... , a0k}
     * {a10, a11, a12, ... , a1k}
     * {a20, a21, a22, ... , a2k}
     * {..., ..., ..., ..., ...}
     * {an0, an1, an2, ... , ank}, {n,k} ∈ N, aij ∈ Z
     *
     * @return multiplication of main diagonal (when i=j)
     */
    public static int findSumOfMainDiagonalOfSquareMatrix(int[][] incomeMatrix) {
        int result = 0;

        for (int i = 0; i < incomeMatrix.length; i++){
            for (int j = 0; j < incomeMatrix[i].length; j++){
                if (i == j) {
                    result += incomeMatrix[i][j];
                }
            }
        }
        return result;
    }

    /**
     * Given a matrix of elements
     * A =
     * {a00, a01, a02, ... , a0k}
     * {a10, a11, a12, ... , a1k}
     * {a20, a21, a22, ... , a2k}
     * {..., ..., ..., ..., ...}
     * {an0, an1, an2, ... , ank}, {n,k} ∈ N, aij ∈ Z
     *
     * @return transposed matrix
     */
    public static int[][] getTransposedMatrix(int[][] incomeMatrix) {
        int[][] transposedMatrix = new int[incomeMatrix[0].length][incomeMatrix.length];
        for (int i = 0; i < incomeMatrix.length; i++){
            for (int j = 0; j < incomeMatrix[0].length; j++){
                transposedMatrix[j][i] = incomeMatrix[i][j];
            }
        }
        return transposedMatrix;
    }

    /**
     * Given a matrix of elements
     * A =
     * {a00, a01, a02, ... , a0k}
     * {a10, a11, a12, ... , a1k}
     * {a20, a21, a22, ... , a2k}
     * {..., ..., ..., ..., ...}
     * {an0, an1, an2, ... , ank}, {n,k} ∈ N, aij ∈ Z
     *
     * and a String with element number
     *
     * @return product of income matrix on it's transposed variant
     */
    public static int[][] multiplyMatrixOnItsTransposedVariant(int[][] incomeMatrix) {
        int[][] transposedMatrix = new int[incomeMatrix[0].length][incomeMatrix.length];
        int[][] result = new int [incomeMatrix.length][transposedMatrix[0].length];
        for (int i = 0; i < incomeMatrix.length; i++) {
            for (int j = 0; j < incomeMatrix[0].length; j++) {
                transposedMatrix[j][i] = incomeMatrix[i][j];
            }
        }
        for (int i = 0; i < incomeMatrix.length; i++){
            for (int j = 0; j < transposedMatrix[0].length; j++){
                for (int k = 0; k < incomeMatrix[0].length; k++){
                    result [i][j] += incomeMatrix[i][k] * transposedMatrix[k][j];
                }
            }
        }
        return result;
    }

}
